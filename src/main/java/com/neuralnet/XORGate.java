package com.neuralnet;

import java.util.Random;

public class XORGate {
    public static final int INPUTS = 2;
    public static final int HIDDEN = 20;
    public static final int OUTPOUTS = 1;

    public static void main(String[] args) throws Exception {
        Neuralnet neuralnet = new Neuralnet(INPUTS, HIDDEN, OUTPOUTS);
        //XOR gate
        for (int i = 0; i < 100_000; i++) {
            double input0 = Math.round(new Random().nextDouble());
            double input1 = Math.round(new Random().nextDouble());
            double[] arr = {input0, input1};
            double output = input0 == input1 ? 0.0 : 1.0;
            double[] arr2 = {output};
            neuralnet.train(arr, arr2);
        }
        double[] test = {1, 1};
        System.out.println(Math.round(neuralnet.feedForward(test).getData()[0][0]));
    }
}
