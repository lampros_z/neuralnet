package com.neuralnet;

import com.neuralnet.FunctionCallables.Sigmoid;
import com.neuralnet.FunctionCallables.SigmoidDerivative;

import java.io.Serializable;

public class Neuralnet implements Serializable {
    private static final boolean LOG_MODE_ON = false;
    private static final int LOG_FREQUENCY = 10_000;

    private int numInputs;
    private int numHidden;
    private int numOutputs;
    private int logCount;
    private Matrix inputs;
    private Matrix hidden;
    private Matrix inputBias;
    private Matrix outputBias;
    private Matrix inputWeights;
    private Matrix outputWeights;

    public Neuralnet(int numInputs, int numHidden, int numOutputs) throws Exception {
        this.numInputs = numInputs;
        this.numHidden = numHidden;
        this.numOutputs = numOutputs;
        this.inputBias = new Matrix(1, this.numHidden);
        this.outputBias = new Matrix(1, this.numOutputs);
        this.inputWeights = new Matrix(numInputs, numHidden);
        this.outputWeights = new Matrix(numHidden, numOutputs);

        this.logCount = LOG_FREQUENCY;

        this.inputWeights.randomWeights();
        this.outputWeights.randomWeights();
        this.inputBias.randomWeights();
        this.outputBias.randomWeights();
    }

    public int getNumInputs() {
        return numInputs;
    }

    public int getNumHidden() {
        return numHidden;
    }

    public int getNumOutputs() {
        return numOutputs;
    }

    public Matrix getInputWeights() {
        return inputWeights;
    }

    public Matrix getOutputWeights() {
        return outputWeights;
    }

    public void setNumInputs(int numInputs) {
        this.numInputs = numInputs;
    }

    public void setNumHidden(int numHidden) {
        this.numHidden = numHidden;
    }

    public void setNumOutputs(int numOutputs) {
        this.numOutputs = numOutputs;
    }

    public void setInputWeights(Matrix inputWeights) {
        this.inputWeights = inputWeights;
    }

    public void setOutputWeights(Matrix outputWeights) {
        this.outputWeights = outputWeights;
    }

    public Matrix feedForward(double[] array) throws Exception {
        // convert input array to a matrix
        inputs = Matrix.convertFromArray(array);

        // find the hidden values and apply the activation function
        hidden = Matrix.dot(inputs, inputWeights);
        hidden = Matrix.add(hidden, inputBias);
        hidden = Matrix.map(hidden, new Sigmoid());

        // find the output values and apply the activation function
        Matrix outputs = Matrix.dot(hidden, outputWeights);
        outputs = Matrix.add(outputs, outputBias);
        outputs = Matrix.map(outputs, new Sigmoid());
        return outputs;
    }

    public void train(double[] inputArray, double[] targetArray) throws Exception {
        // feed the input data through the network
        Matrix outputs = feedForward(inputArray);

        // calculate the output errors (target - output)
        Matrix targets = Matrix.convertFromArray(targetArray);
        Matrix outputsErrors = Matrix.sub(targets, outputs);
        if(LOG_MODE_ON){
            if(logCount == LOG_FREQUENCY){
                System.out.println("Outuput error: "+outputsErrors);
            }
            logCount--;
            if(logCount == 0){
                logCount = LOG_FREQUENCY;
            }
        }
        // calculate the deltas (errors * derivitive of the output)
        Matrix outputDerivs = Matrix.map(outputs, new SigmoidDerivative());
        Matrix outputDeltas = Matrix.mult(outputsErrors, outputDerivs);

        // calculate hidden layer errors (deltas "dot" transpose of weights1)
        Matrix outputWeightsTransposed = Matrix.transpose(this.outputWeights);
        Matrix hiddenErrors = Matrix.dot(outputDeltas, outputWeightsTransposed);

        // calculate the hidden deltas (errors * derivitive of hidden)
        Matrix hiddenDerivs = Matrix.map(hidden, new SigmoidDerivative());
        Matrix hiddenDeltas = Matrix.mult(hiddenErrors, hiddenDerivs);

        // update the weights (add transpose of layers "dot" deltas)
        Matrix hiddenTranspose = Matrix.transpose(hidden);
        this.outputWeights = Matrix.add(this.outputWeights, Matrix.dot(hiddenTranspose, outputDeltas));
        Matrix inputsTransposed = Matrix.transpose(inputs);
        this.inputWeights = Matrix.add(this.inputWeights, Matrix.dot(inputsTransposed, hiddenDeltas));

        //update bias
        this.outputBias = Matrix.add(this.outputBias, outputDeltas);
        this.inputBias = Matrix.add(this.inputBias, hiddenDeltas);
    }
}
