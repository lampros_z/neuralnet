package com.neuralnet;

import java.io.*;
import java.util.Random;

public class Adder {
    public static final int INPUTS = 2;
    public static final int HIDDEN = 100;
    public static final int OUTPOUTS = 1;

    public static void main(String[] args) throws Exception {
//        Neuralnet neuralnet = new Neuralnet(INPUTS, HIDDEN, OUTPOUTS);
//        Random random = new Random();
        Neuralnet neuralnet;
        try( var is = new ObjectInputStream(new BufferedInputStream(new FileInputStream("neuralnet")))){
            neuralnet = (Neuralnet) is.readObject();
        }
//        for (int i = 0; i < 1_000_000; i++) {
//            double input0 = new Random().nextDouble();
//            double input1 = new Random().nextDouble();
//            double[] arr = {input0, input1};
//            double output = input0 + input1;
//            double[] arr2 = {output};
//            neuralnet.train(arr, arr2);
//        }
//        try(var os = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream("neuralnet")))){
//            os.writeObject(neuralnet);
//        }
        double[] test = {0.23, 0.22};
        var result = neuralnet.feedForward(test).getData()[0][0];
        System.out.println(Math.round(result*100.0)/100.0);
    }
}
