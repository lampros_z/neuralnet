package com.neuralnet;

import com.neuralnet.FunctionCallables.Function;

import java.io.Serializable;
import java.util.Random;

public class Matrix implements Serializable {
    private int rows;
    private int columns;
    private double[][] data;

    public Matrix(int rows, int columns) throws Exception {
        this.rows = rows;
        this.columns = columns;

        if(this.data == null || this.data.length == 0){
            this.data = new double[rows][columns];
            for (int i = 0; i < this.data.length; i++) {
                for (int j = 0; j < this.data[i].length; j++) {
                    this.data[i][j] = 0;
                }
            }
        }
    }

    public Matrix(int rows, int columns, double[][] data) throws Exception {
        this.rows = rows;
        this.columns = columns;
        this.data = data;
        if(this.data.length != data.length)
            throw new Exception("Invalid matrix initialization.");
        else{
            for (int i = 0; i < this.data.length; i++) {
                if(this.data[i].length != columns)
                    throw new Exception("Invalid matrix initialization.");
            }
        }
    }

    public int getRows() {
        return rows;
    }

    public int getColumns() {
        return columns;
    }

    public double[][] getData() {
        return data;
    }

    //random wights between -1 and 1
    public void randomWeights(){
        Random random = new Random();
        for (int i = 0; i < data.length; i++) {
            for (int j = 0; j < data[i].length; j++) {
                data[i][j] = random.nextDouble() * 2 -1;
            }
        }
    }

    public static Matrix add(Matrix m0, Matrix m1) throws Exception {
        checkDimensions(m0, m1);
        int rows = m0.rows;
        int columns = m0.columns;
        Matrix matrix = new Matrix(rows, columns);
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                matrix.data[i][j] = m0.data[i][j] + m1.data[i][j];
            }
        }
        return matrix;
    }

    public static Matrix sub(Matrix m0, Matrix m1) throws Exception {
        checkDimensions(m0, m1);
        int rows = m0.rows;
        int columns = m0.columns;
        Matrix matrix = new Matrix(rows, columns);
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                matrix.data[i][j] = m0.data[i][j] - m1.data[i][j];
            }
        }
        return matrix;
    }

    public static Matrix mult(Matrix m0, Matrix m1) throws Exception {
        checkDimensions(m0, m1);
        int rows = m0.rows;
        int columns = m0.columns;
        Matrix matrix = new Matrix(rows, columns);
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                matrix.data[i][j] = m0.data[i][j] * m1.data[i][j];
            }
        }
        return matrix;
    }

    public static Matrix dot(Matrix m0, Matrix m1) throws Exception {
        checkDotCompatibility(m0, m1);
        int rows = m0.rows;
        int columns = m1.columns;
        Matrix matrix = new Matrix(rows, columns);
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                double sum = 0;
                for (int k = 0; k < m0.columns; k++) {
                    sum += m0.data[i][k] * m1.data[k][j];
                }
                matrix.data[i][j] = sum;
            }
        }
        return matrix;
    }

    public static Matrix transpose(Matrix m) throws Exception {

        Matrix matrix = new Matrix(m.columns, m.rows);
        for (int i = 0; i < m.rows; i++) {
            for (int j = 0; j < m.columns; j++) {
                matrix.data[j][i] = m.data[i][j];
            }
        }
        return matrix;
    }

    public static Matrix convertFromArray(double[] array) throws Exception {
        double data[][] = new double[1][array.length];
        for (int i = 0; i < array.length; i++) {
            data[0][i] = array[i];
        }
        return new Matrix(1, array.length, data);
    }

    public static Matrix map(Matrix m, Function function){
        return function.transform(m);
    }

    private static void checkDimensions(Matrix m0, Matrix m1) throws Exception {
        if(m0.rows != m1.rows || m0.columns != m1.columns)
            throw new Exception("Cannot add matrices of different dimensions.");
    }
    private static void checkDotCompatibility(Matrix m0, Matrix m1) throws Exception {
        if( m0.columns != m1.rows )
            throw new Exception("Column length of m0 should be equal to the row length of m1");
    }

    @Override
    public String toString() {
        StringBuilder sb  = new StringBuilder();
        for (int i = 0; i < data.length; i++) {
            for (int j = 0; j < data[i].length; j++) {
                sb.append(" "+data[i][j]);
            }
            sb.append("\n");
        }
        return sb.toString();
    }
}
