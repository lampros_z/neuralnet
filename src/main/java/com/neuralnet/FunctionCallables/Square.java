package com.neuralnet.FunctionCallables;

import com.neuralnet.Matrix;

public class Square implements Function {
    @Override
    public Matrix transform(Matrix matrix) {
        for (int i = 0; i < matrix.getRows(); i++) {
            for (int j = 0; j < matrix.getColumns(); j++) {
                matrix.getData()[i][j] = matrix.getData()[i][j] * matrix.getData()[i][j];
            }
        }
        return matrix;
    }
}
