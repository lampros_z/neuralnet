package com.neuralnet.FunctionCallables;

import com.neuralnet.Matrix;

public class Sigmoid implements Function{

    @Override
    public Matrix transform(Matrix matrix) {
        for (int i = 0; i < matrix.getRows(); i++) {
            for (int j = 0; j < matrix.getColumns(); j++) {
                matrix.getData()[i][j] = sigmoid(matrix.getData()[i][j]);
            }
        }
        return matrix;
    }

    private double sigmoid(double x){
        return 1 / (1 + Math.exp(-x));
    }
}
