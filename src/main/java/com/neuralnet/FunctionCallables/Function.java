package com.neuralnet.FunctionCallables;

import com.neuralnet.Matrix;

public interface Function {
    Matrix transform(Matrix matrix);
}
