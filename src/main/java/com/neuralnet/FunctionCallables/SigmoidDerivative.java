package com.neuralnet.FunctionCallables;

import com.neuralnet.Matrix;

public class SigmoidDerivative implements Function{
    @Override
    public Matrix transform(Matrix matrix) {
        for (int i = 0; i < matrix.getRows(); i++) {
            for (int j = 0; j < matrix.getColumns(); j++) {
                matrix.getData()[i][j] = sigmoidDeriv(matrix.getData()[i][j]);
            }
        }
        return matrix;
    }


    private double sigmoidDeriv(double x){
        //x=sigmoid(x)
        return x * (1 - x);
    }
}
